const commands = {
  keys: {
    76: 'LOOK',
    74: 'GET',
    75: 'SCAVENGE',
  }
}

commands['allKeys'] = Array.from(Object.keys(commands.keys));

module.exports = commands;
