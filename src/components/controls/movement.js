const movement = {
  keys: {
    87: 'UP',
    38: 'UP',
    83: 'DOWN',
    40: 'DOWN',
    65: 'LEFT',
    37: 'LEFT',
    68: 'RIGHT',
    39: 'RIGHT'
  },
  directions: {
    'UP': {x: 0, y: -1},
    'DOWN': {x: 0, y: 1},
    'LEFT': {x: -1, y: 0},
    'RIGHT': {x: 1, y: 0}
  }
}

movement['allKeys'] = Array.from(Object.keys(movement.keys));

module.exports = movement;
