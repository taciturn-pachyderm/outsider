import React from 'react';

import style from './style';

export class HintDisplay extends React.Component {

  render = () => {
    return <div style={ style.hint }>
      <div>
        { this.props.hint }
      </div>
    </div>
  }
}
