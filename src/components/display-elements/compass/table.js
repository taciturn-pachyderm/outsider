/* eslint react/prop-types: 0 */
import * as React from 'react';

import { Row } from './row';
import KNOBS_AND_LEVERS from '../../defaults/knobs-and-levers'

export class Table extends React.Component {

  render() {
    const squares = this.props.squares;
    var rows = [];

    for (let i = 0; i < squares.length; ++i) {
      if(Math.abs(i - this.props.position.y) < KNOBS_AND_LEVERS.compassVisibility) {
        rows.push(Row(squares[i], i, this.props.style, this.props.position.x));
      }
    }

    return (
      <div>
        {rows}
      </div>
    );
  }
}
