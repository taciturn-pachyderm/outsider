import * as React from 'react';

import { Table } from './table';
import style from './style';

export class CompassDisplay extends React.Component {

  render() {
    const squares = this.props.squares.concat();
    return (<div style={ style.compassWrapper }>
        <Table
          position={this.props.position}
          squares={squares}
          style={style}
        />
      </div>
    );
  }
}
