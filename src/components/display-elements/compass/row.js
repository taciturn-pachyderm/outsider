import * as React from 'react';

import { Square } from './square';
import KNOBS_AND_LEVERS from '../../defaults/knobs-and-levers'

export const Row = (squareRow, rowIndex, style, compassLimiter) =>
  <div key={rowIndex} style={style.row}>
    { squareRow.map((square, index) =>
      Math.abs(index - compassLimiter) < KNOBS_AND_LEVERS.compassVisibility
        ? <Square
            style={style} square={square}
            key={ square.coordinates ? square.coordinates : Math.random(Date.now()) }
          />
        : null
    )}
  </div>;
