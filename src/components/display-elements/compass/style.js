const style = {
  compassWrapper: {
    position: 'absolute',
    top: '100px',
    left: '502px',
    width: '200px',
    height: '196px',
    borderTop: '1px solid black',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    paddingTop: '6px',
    backgroundColor: '#DDD'
    // overflow: 'hidden',
  },
  row: {
    whiteSpace: 'nowrap',
    marginTop: '-4px',
  },
  activeSquare: {
    border: '1px solid #EEE',
    fontSize: '12px',
    fontWeight: 'bold',
    lineHeight: '26px',
    height: '26px',
    width: '26px',
    textAlign: 'center',
    overflow: 'hidden',
    display: 'inline-block',
    backgroundColor: '#FFF',
  },
  inactiveSquare: {
    border: '1px solid #999',
    height: '26px',
    width: '26px',
    textAlign: 'center',
    overflow: 'hidden',
    display: 'inline-block',
    backgroundColor: '#999',
  },
  coordinates: {
    float: 'right',
    paddingRight: '10px',
  }
}

module.exports = style;
