import * as React from 'react';

export const Square = props =>
  <div style={ props.square.inactive ? props.style.inactiveSquare : props.style.activeSquare }>
    {props.square.playerSymbol}{props.square.type}
    {/* ({props.square.x},{props.square.y}) */}
  </div>;
