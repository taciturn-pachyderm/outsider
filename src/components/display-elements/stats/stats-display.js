import React from 'react';

import style from './style';

export class StatsDisplay extends React.Component {

  render = () => {
    return <div style={ style.stats }>
      <div>
        light: { this.props.stats.light } | health: { this.props.stats.health }
      </div>
      <div>coordinates: {this.props.position.x + ', ' + this.props.position.y}</div>
    </div>
  }
}
