import React from 'react';

import { Message } from './message';
import style from './style';

export class LogDisplay extends React.Component {

  render = () => {
    return <div style={ style.log }>
      <div>
        { this.props.log.map( logEntry => <Message message={logEntry} key={Math.random(Date.now())} /> ) }
      </div>
    </div>
  }
}
