/* eslint react/prop-types: 0 */
import * as React from 'react';

import { Row } from './atlas-row';

export class Table extends React.Component {

  render() {
    const squares = this.props.squares;
    var rows = [];

    for (let i = 0; i < squares.length; ++i) {
      rows.push(Row(squares[i], i, this.props.style));
    }

    return (
      <div>
        {rows}
      </div>
    );
  }
}
