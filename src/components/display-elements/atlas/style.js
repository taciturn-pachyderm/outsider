const size = 5;

const style = {
  atlas: {
    position: 'absolute',
    top: '301px',
    left: '501px',
    width: '501px',
    height: '393px',
    // overflow: 'hidden',
    padding: '6px 0 0 2px',
    border: '1px solid black'
  },
  row: {
    whiteSpace: 'nowrap',
    marginTop: -2 * size + 'px',
  },
  activeSquare: {
    border: '1px solid #EEE',
    fontSize: 0.75 * size + 'px',
    fontWeight: 'bold',
    lineHeight: size + 'px',
    height: size + 'px',
    width: size + 'px',
    textAlign: 'center',
    overflow: 'hidden',
    display: 'inline-block',
  },
  inactiveSquare: {
    border: '1px solid #999',
    height: size + 'px',
    width: size + 'px',
    textAlign: 'center',
    overflow: 'hidden',
    display: 'inline-block',
    backgroundColor: '#999',
  }
}

module.exports = style;
