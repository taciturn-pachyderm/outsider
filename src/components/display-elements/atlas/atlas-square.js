import * as React from 'react';

export const Square = props =>
  <div style={ props.square.inactive || !props.square.visited ? props.style.inactiveSquare : props.style.activeSquare }>
    {props.square.playerSymbol}
    {/* ({props.square.visited ? 'y' : 'n'},{props.square.x},{props.square.y}) */}
  </div>;
