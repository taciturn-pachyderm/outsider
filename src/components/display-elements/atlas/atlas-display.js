import React from 'react';

import { Table } from './atlas-table';
import style from './style';

export class AtlasDisplay extends React.Component {

  render() {
    const squares = this.props.squares.concat();
    return (<div style={ style.atlas }>
        <Table
          position={this.props.position}
          squares={squares}
          style={style}
        />
      </div>
    );
  }
}
