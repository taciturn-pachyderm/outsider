import * as React from 'react';

import { Square } from './atlas-square';

export const Row = (squareRow, rowIndex, style) =>
  <div key={rowIndex} style={style.row}>
    { squareRow.map(square =>
      <Square
        style={style} square={square}
        key={ square.coordinates ? square.coordinates : Math.random(Date.now()) }
      />
    )}
  </div>;
