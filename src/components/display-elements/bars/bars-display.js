import React from 'react';

import style from './style';

export class BarsDisplay extends React.Component {

  render = () => {
    return <div style={ style.bars }>
      <div>
        light: { this.props.stats.light } | health: { this.props.stats.health }
      </div>
    </div>
  }
}
