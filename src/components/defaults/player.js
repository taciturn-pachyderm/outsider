const playerDefaults = {
  stats: {
    light: 200000,
    health: 10
  }
}

module.exports = playerDefaults;
