const knobsAndLevers = {
  maxLogArraySize: 35,
  mapDimensions: {
    height: 44, width: 53
  },
  playerSymbol: 'O',
  hint: 'must find light...',
  compassVisibility: 4 /* min:2, max:4 */,
}

module.exports = knobsAndLevers;
