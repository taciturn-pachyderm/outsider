const roomDefaults = {

  '6,6': {
    name: 'Room name',
    description: 'the mounded shapes of store sacks are visibile in the flickering light',
    items: [
      { name: 'torch', value: 3 },
      { name: 'flint', value: 4 },
      { name: 'tinder', value: 2 },
      { name: 'kindling', value: 5 }
    ]
  },
  '6,7': {
    name: 'Room name',
    description: 'a hearth dominates a nearby wall, the ashes cold',
    items: [{ name: 'a dusty map' }],
    interactables: ['hearth']
  },
  '7,7': { name: 'Room name', description: 'room description' },
  '7,8': { name: 'Room name', description: 'room description' },

  '8,8': { name: 'Room name', description: 'room description' },
  '8,9': { name: 'Room name', description: 'room description' },
  '8,10': { name: 'Room name', description: 'room description' },
  '8,11': { name: 'Room name', description: 'room description' },

  '9,11': { name: 'Room name', description: 'room description' },
  '9,12': { name: 'Room name', description: 'room description' },
  '9,13': { name: 'Room name', description: 'room description' },

  '10,8': { name: 'Room name', description: 'room description' },
  '10,9': { name: 'Room name', description: 'room description' },
  '10,10': { name: 'Room name', description: 'room description' },
  '10,11': { name: 'Room name', description: 'room description' },

  '11,8': { name: 'Room name', description: 'room description' },
  '11,9': { name: 'Room name', description: 'room description' },
  '11,11': { name: 'Room name', description: 'room description' },

  '12,9': { name: 'Room name', description: 'room description' },
  '12,11': { name: 'Room name', description: 'room description' },

  '13,7': { name: 'Room name', description: 'room description' },
  '13,8': { name: 'Room name', description: 'room description' },
  '13,9': { name: 'Room name', description: 'room description' },
  '13,11': { name: 'Room name', description: 'room description' },

  '14,11': { name: 'Room name', description: 'room description' },

  '14,12': { name: 'Room name', description: 'room description' },

  '15,12': { name: 'Room name', description: 'room description' },
  '15,13': { name: 'Room name', description: 'room description' },

}

module.exports = roomDefaults;
