import MOVEMENT from '../controls/movement'
import COMMANDS from '../controls/commands'
import TEXT from '../assets/text'
import ROOMS from '../defaults/rooms'
import KNOBS_AND_LEVERS from '../defaults/knobs-and-levers';
import PLAYER_DEFAULTS from '../defaults/player';

import { updateLog } from './log-manager'

var inactiveRoom = {inactive: true, visited: false};

var assignRooms = (arr) => {
  var coordinates = []
  Array.from(Object.keys(ROOMS)).forEach((element) => {
    coordinates = element.split(',');
    arr[coordinates[1]][coordinates[0]] = ROOMS[element];
    arr[coordinates[1]][coordinates[0]].x = coordinates[0];
    arr[coordinates[1]][coordinates[0]].y = coordinates[1];
  });
}

export function constructSquaresArray() {
  let arr = Array(KNOBS_AND_LEVERS.mapDimensions.height);
  for (let i = 0; i < arr.length; ++i) {
    let arrInner = new Array(KNOBS_AND_LEVERS.mapDimensions.width);
    arr[i] = arrInner;
    var anInactiveRoom = Object.create(inactiveRoom);
    anInactiveRoom.y = i;
    arr[i].fill(anInactiveRoom);
  }
  assignRooms(arr);
  return arr;
}

// access order is not logical here; this is due to how the array gets built
// squares 'coordinates' are read as y,x
export function getAvailablePositions(squares, position) {
  return {
    'UP':
      position.y > 0
        ? squares[position.y - 1][position.x]
        : inactiveRoom,
    'DOWN':
      position.y < KNOBS_AND_LEVERS.mapDimensions.height - 1
        ? squares[position.y + 1][position.x]
        : inactiveRoom,
    'LEFT':
      position.x > 0
        ? squares[position.y][position.x - 1]
        : inactiveRoom,
    'RIGHT':
      position.x < KNOBS_AND_LEVERS.mapDimensions.width - 1
        ? squares[position.y][position.x + 1]
        : inactiveRoom
  }
}

export function updatePosition(position, direction, availablePositions) {
  if(PLAYER_DEFAULTS.stats.light <= 0) {
    updateLog('the light has faded, the shadows deepen...');
    return position;
  }
  if (availablePositions[direction] && !availablePositions[direction].inactive) {
    position.y += MOVEMENT.directions[direction].y;
    position.x += MOVEMENT.directions[direction].x;
    updateLog(TEXT.movement[direction]);
    PLAYER_DEFAULTS.stats.light -= 1;
  };
  return position;
}

export function getUpdatedSquareState(squares, oldPosition, newPosition, direction) {
  if (newPosition.x !== oldPosition.x || newPosition.y !== oldPosition.y) {
    squares[newPosition.y][newPosition.x].playerSymbol = KNOBS_AND_LEVERS.playerSymbol;
    squares[newPosition.y][newPosition.x].visited = true;
    squares[oldPosition.y][oldPosition.x].playerSymbol = '';
  } else {
    if(PLAYER_DEFAULTS.stats.light <= 0) {
      return squares;
    }
    updateLog(TEXT.noMovement[direction]);
  };
  return squares;
}

var getItems = (items) => {
  return items ? "there are items amongst the refuse: " + items.map( item => item.name).join(", ") : "";
}

export function executeCommand(keyCode, square) {
  let command = COMMANDS.keys[keyCode];
  if(command === 'LOOK') {
    updateLog(square.description);
  };
  if(command === 'SCAVENGE') {
    updateLog(getItems(square.items));
  };
}
