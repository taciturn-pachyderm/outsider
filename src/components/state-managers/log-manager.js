import KNOBS_AND_LEVERS from '../defaults/knobs-and-levers';

export const LOG = []

export function updateLog(message) {
  LOG.unshift(message);
  if (LOG.length > KNOBS_AND_LEVERS.maxLogArraySize) {
    LOG.pop();
  };
}
