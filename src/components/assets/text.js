function template(strings, ...keys) {
  return (function(...values) {
    var dict = values[values.length - 1] || {};
    var result = [strings[0]];
    keys.forEach(function(key, i) {
      var value = Number.isInteger(key) ? values[key] : dict[key];
      result.push(value, strings[i + 1]);
    });
    return result.join('');
  });
}

const templates = {
  moveSuccess: template`shadows give way before the torchlight`,
  moveFailure: template`passage is impossible; the way is shut`
}

const text = {
  movement: {
    UP: templates.moveSuccess('north'),
    DOWN: templates.moveSuccess('south'),
    RIGHT: templates.moveSuccess('east'),
    LEFT: templates.moveSuccess('west'),
  },
  noMovement: {
    UP: templates.moveFailure('north'),
    DOWN: templates.moveFailure('south'),
    RIGHT: templates.moveFailure('east'),
    LEFT: templates.moveFailure('west'),
  }
}

module.exports = text;
