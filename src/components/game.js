import React from 'react';

import style from './style';
import { CompassDisplay } from './display-elements/compass/compass-display';
import { LogDisplay } from './display-elements/log/log-display';
import { HintDisplay } from './display-elements/hint/hint-display';
import { StatsDisplay } from './display-elements/stats/stats-display';
import { BarsDisplay } from './display-elements/bars/bars-display';
import { AtlasDisplay } from './display-elements/atlas/atlas-display';

import KNOBS_AND_LEVERS from './defaults/knobs-and-levers';
import PLAYER_DEFAULTS from './defaults/player';
import MOVEMENT from './controls/movement';
import COMMANDS from './controls/commands';
import { LOG } from './state-managers/log-manager';

import { constructSquaresArray } from './state-managers/room-manager';
import { getAvailablePositions } from './state-managers/room-manager';
import { updatePosition } from './state-managers/room-manager';
import { getUpdatedSquareState } from './state-managers/room-manager';
import { executeCommand } from './state-managers/room-manager';

export class Game extends React.Component {
  constructor(props) {
    super();
    let squares = constructSquaresArray();
    let position = {x: 6, y: 6};
    squares[position.y][position.x].playerSymbol = KNOBS_AND_LEVERS.playerSymbol;
    squares[position.y][position.x].visited = true;

    this.state = {
      log: LOG,
      hint: KNOBS_AND_LEVERS.hint,
      position: position,
      squares: squares,
    }
  };

  componentWillMount() {
    document.addEventListener('keydown', this.handleKeyPress.bind(this));
  }

  processMovement = (keyCode) => {
    // shallow clone
    let newPosition = Object.assign({}, this.state.position);
    let availablePositions = getAvailablePositions(this.state.squares.slice(), newPosition);
    let directionToMove = MOVEMENT.keys[keyCode];
    newPosition = updatePosition(newPosition, directionToMove, availablePositions);
    let squares = this.state.squares.slice();
    let oldPosition = this.state.position;
    squares = getUpdatedSquareState(squares, oldPosition, newPosition, directionToMove);
    this.setState({
      squares: squares,
      position: newPosition,
    });
  }

  processCommand = (keyCode) => {
    let squares = this.state.squares.slice();
    let position = this.state.position;
    executeCommand(keyCode, squares[position.y][position.x]);
    this.setState({
      squares: squares,
    });
  }

  handleKeyPress = (event) => {
    if(PLAYER_DEFAULTS.stats.light < 0) {
      return;
    }
    if(MOVEMENT.allKeys.includes("" + event.keyCode)) {
      this.processMovement(event.keyCode);
      return;
    }
    if(COMMANDS.allKeys.includes("" + event.keyCode)) {
      this.processCommand(event.keyCode);
      return;
    }
  }

  render() {
    console.log(this.state.quares);
    return <div style={ style.windowWrapper }>
      <LogDisplay log={this.state.log} />
      <CompassDisplay position={this.state.position} squares={this.state.squares} />
      <StatsDisplay stats={PLAYER_DEFAULTS.stats} position={this.state.position} />
      {/* <AtlasDisplay position={this.state.position} squares={this.state.squares} /> */}
      <BarsDisplay stats={PLAYER_DEFAULTS.stats} />
      <HintDisplay hint={this.state.hint} />
    </div>
  }
}
