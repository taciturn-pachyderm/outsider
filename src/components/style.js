const style = {
  windowWrapper: {
    margin: 'auto',
    width: '1000px',
    position: 'relative'
  },
  stats: {
    position: 'absolute',
    top: '100px',
    left: '702px',
    width: '300px',
    height: '200px',
    overflow: 'hidden',
    border: '1px solid black'
  },
  atlas: {
    position: 'absolute',
    top: '301px',
    left: '501px',
    width: '501px',
    height: '399px',
    overflow: 'hidden',
    border: '1px solid black'
  },
  bars: {
    position: 'absolute',
    top: '701px',
    left: '0px',
    width: '675px',
    height: '20px',
    overflow: 'hidden',
    border: '1px solid black'
  },
  hint: {
    position: 'absolute',
    top: '701px',
    left: '676px',
    width: '326px',
    height: '20px',
    overflow: 'hidden',
    border: '1px solid black'
  }
}

module.exports = style;
